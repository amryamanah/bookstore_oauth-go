module bitbucket.org/amryamanah/bookstore_oauth-go

go 1.15

require (
	github.com/amryamanah/bookstore_utils-go v0.0.1
	github.com/federicoleon/golang-restclient v0.0.0-20191104170228-162ed620df66
)
